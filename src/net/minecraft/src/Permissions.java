package net.minecraft.src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import net.minecraft.server.MinecraftServer;

public class Permissions {
	private static ArrayList<String> getPermissionsOfPlayer(String player) throws IOException {
		if(!new File("permissions/" + player + ".txt").exists())
			new File("permissions/" + player + ".txt").createNewFile();
		
		ArrayList<String> perms = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(new File("permissions/" + player + ".txt")));
		String line = null;
		while((line = reader.readLine()) != null)
			perms.add(line);
		
		reader.close();
		return perms;
	}
	
	public static boolean hasPermission(String player, String node) {
		if(MinecraftServer.getServer().getConfigurationManager().getOps().contains(player)) return true;
		try {
			if(getPermissionsOfPlayer(player) == null) return false;
			
			boolean has = getPermissionsOfPlayer(player).contains(node);
			return has;
		} catch (IOException e) {
			return false;
		}
	}
	
	public static void addPermission(String player, String node) throws IOException {
		if(!new File("permissions/" + player + ".txt").exists())
			new File("permissions/" + player + ".txt").createNewFile();
		
		ArrayList<String> perms = getPermissionsOfPlayer(player);
		perms.add(node);
		PrintWriter out = new PrintWriter("permissions/" + player + ".txt");
		for(String perm : perms)
			out.println(perm);
		out.close();
	}
	
	public static void removePermission(String player, String node) throws IOException {
		if(!new File("permissions/" + player + ".txt").exists())
			new File("permissions/" + player + ".txt").createNewFile();
		
		ArrayList<String> perms = getPermissionsOfPlayer(player);
		perms.remove(node);
		for(String perm : perms) {
			PrintWriter out = new PrintWriter("permissions/" + player + ".txt");
			out.println(perm);
			out.close();
		}
	}
}