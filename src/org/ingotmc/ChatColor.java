package org.ingotmc;

public class ChatColor {
	public static String RED = "\u00A7c", DARK_RED = "\u00A74", BLUE = "\u00A79", DARK_BLUE = "\u00A71", PINK = "\u00A7d", PURPLE = "\u00A75", RESET = "\u00A7r", GRAY = "\u00A77", DARK_GRAY = "\u00A78", MAGIC = "\u00A7k", BOLD = "\u00A7l", ITALIC = "\u00A7o", UNDERLINED = "\u00A7n", GREEN = "\u00A7a", DARK_GREEN = "\u00A72", CYAN = "\uA0073", LIGHT_BLUE = "\u00A7b", GOLD = "\u00A76";
}
