package org.ingotmc.api;

public class Block {
	private Location l;
	
	public Block(Location l) {
		this.l = l;
	}
	
	public void setType(int id) {
		l.setBlock(id);
	}
	
	public int getType() {
		return l.getBlockType();
	}
	
	public Location getLocation() {
		return l;
	}
}
