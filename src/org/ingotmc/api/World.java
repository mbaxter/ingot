package org.ingotmc.api;

public class World {
	private net.minecraft.src.World w;
	
	public World(net.minecraft.src.World w) {
		this.w = w;
	}
	
	public String getName() {
		return w.getWorldInfo().getWorldName();
	}
	
	public long getTime() {
		return w.getWorldTime();
	}
	
	public void setTime(long time) {
		w.setWorldTime(time);
	}
	
	public void setBlock(Location l, int id) {
		w.setBlock(l.getBlockX(), l.getBlockY(), l.getBlockZ(), id);
	}
	
	public int getBlockType(Location l) {
		return w.getBlockId(l.getBlockX(), l.getBlockY(), l.getBlockZ());
	}
	
	public net.minecraft.src.World getNMS() {
		return w;
	}
}
