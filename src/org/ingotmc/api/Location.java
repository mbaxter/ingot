package org.ingotmc.api;

public class Location {
	private double x, y, z;
	private World w;
	private float pitch, yaw;
	
	public Location(World w, double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public Location(World w, double x, double y, double z, float yaw, float pitch) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		this.pitch = pitch;
		this.yaw = yaw;
	}
	
	public Location(World w, int x, int y, int z, float yaw, float pitch) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		this.pitch = pitch;
		this.yaw = yaw;
	}
	
	public Location(World w, int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public int getBlockX() {
		return (int) x;
	}
	
	public int getBlockY() {
		return (int) y;
	}
	
	public int getBlockZ() {
		return (int) z;
	}
	
	public Float getPitch() {
		return pitch;
	}
	
	public Float getYaw() {
		return yaw;
	}
	
	public void setBlock(int id) {
		getWorld().setBlock(this, id);
	}
	
	public int getBlockType() {
		return getWorld().getBlockType(this);
	}
	
	public World getWorld() {
		return w;
	}
}
