package org.ingotmc.api;

public enum GameMode {
	CREATIVE, SURVIVAL, ADVENTURE, UNKNOWN;
	
	public static GameMode getById(int id) {
		if(id == 1)
			return CREATIVE;
		else if(id == 0)
			return SURVIVAL;
		else if(id == 2)
			return ADVENTURE;
		else
			return UNKNOWN;
	}
}
