package org.ingotmc.api;

import net.minecraft.src.ChatMessageComponent;
import net.minecraft.src.ICommandSender;
import net.minecraft.src.Permissions;

public class CommandSender {
	private ICommandSender s;
	
	public String getName() {
		return s.getCommandSenderName();
	}
	
	public boolean isPlayer() {
		return (!getName().equalsIgnoreCase("Server"));
	}
	
	public ICommandSender getNMS() {
		return s;
	}
	
	public boolean hasPermission(String perm) {
		return Permissions.hasPermission(getName(), perm);
	}
	
	public void sendMessage(String m) {
		if(!isPlayer()) {
			for(char c : m.toCharArray()) {
				if(Character.toString(c).equals("\u00A7")) {
					int i = m.indexOf(Character.toString(c));
					m = m.replaceFirst(Character.toString(m.charAt(i)), "");
					m = m.replaceFirst(Character.toString(m.charAt(i)), "");
					sendMessage(m);
					return;
				}
			}
		}
		s.sendChatToPlayer(ChatMessageComponent.func_111077_e(m));
	}
	
	public CommandSender(ICommandSender s) {
		this.s = s;
	}
}
