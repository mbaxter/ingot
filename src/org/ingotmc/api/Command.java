package org.ingotmc.api;

import java.util.List;

import org.ingotmc.ChatColor;


import net.minecraft.src.CommandBase;
import net.minecraft.src.ICommandSender;

public abstract class Command extends CommandBase {
	public abstract String getCommandName();
	public abstract int getRequiredArgumentsLength();
	public abstract String getCommandUsage();
	public abstract void processCommand(CommandSender s, String[] a);
	public abstract List<String> getCommandAliases();
	
	public String getCommandDescription() {
		return ChatColor.ITALIC + "No command description";
	}
	
	@Override
	public String getCommandUsage(ICommandSender var1) {
		return getCommandUsage();
	}
	
	public boolean canCommandSenderUseCommand(ICommandSender var1) {
		return true;
	}

	@Override
	public void processCommand(ICommandSender var1, String[] var2) {
		CommandSender s = new CommandSender(var1);
		if(var2.length < getRequiredArgumentsLength())
			s.sendMessage("\u00A7cUsage: " + getCommandUsage());
		else
			processCommand(s, var2);
	}
}
