package org.ingotmc.api;

import java.io.IOException;

import org.ingotmc.exceptions.UnknownGameModeException;

import net.minecraft.server.MinecraftServer;
import net.minecraft.src.ChatMessageComponent;
import net.minecraft.src.EntityPlayerMP;
import net.minecraft.src.EnumGameType;
import net.minecraft.src.ItemStack;
import net.minecraft.src.Permissions;

public class Player extends LivingEntity {
	private EntityPlayerMP pl;
	
	public Player(EntityPlayerMP mp) {
		pl = mp;
	}
	
	public EntityPlayerMP getNMS() {
		return pl;
	}
	
	public String getName() {
		return pl.getCommandSenderName();
	}
	
	public void sendMessage(String m) {
		pl.sendChatToPlayer(ChatMessageComponent.func_111077_e(m));
	}
	
	public String getIP() {
		return pl.getPlayerIP();
	}
	
	public Location getLocation() {
		return new Location(new World(pl.worldObj), pl.posX, pl.posY, pl.posZ, pl.cameraYaw, pl.cameraPitch);
	}
	
	@Deprecated
	public void teleport(Location l) {
		pl.setPositionAndUpdate(l.getX(), l.getY(), l.getZ());
	}
	
	public void setLocation(Location l) {
		pl.setLocationAndAngles(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
	}
	
	public int getArrowCount() {
		return pl.getArrowCountInEntity();
	}
	
	public int getChatVisibility() {
		return pl.getChatVisibility();
	}
	
	@Deprecated
	public World getWorld() {
		return new World(pl.worldObj);
	}
	
	public boolean isOp() {
		return MinecraftServer.getServer().getConfigurationManager().getOps().contains(getName());
	}
	
	public boolean getFlying() {
		return pl.capabilities.isFlying;
	}
	
	public boolean getFlyingAllowed() {
		return pl.capabilities.allowFlying;
	}
	
	@Deprecated
	public boolean isCreativeMode() {
		return pl.capabilities.isCreativeMode;
	}
	
	public ItemStack getHeldItem() {
		return pl.getHeldItem();
	}
	
	public boolean hasPermission(String perm) {
		return Permissions.hasPermission(getName(), perm);
	}
	
	public void addPermission(String perm) {
		try {
			Permissions.addPermission(getName(), perm);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void takePermission(String perm) {
		try {
			Permissions.removePermission(getName(), perm);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setGameMode(GameMode mode) throws UnknownGameModeException {
		if(mode.equals(GameMode.UNKNOWN))
			throw new UnknownGameModeException();
		pl.setGameType(EnumGameType.valueOf(mode.toString()));
	}
	
	public GameMode getGameMode() {
		if(pl.capabilities.isCreativeMode) return GameMode.CREATIVE;
		return GameMode.UNKNOWN;
	}
	
	public void setHealth(float health) {
		pl.setEntityHealth(health);
	}
}
