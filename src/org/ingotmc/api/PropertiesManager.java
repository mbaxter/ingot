package org.ingotmc.api;

import net.minecraft.src.DedicatedServer;

public class PropertiesManager {
	public static boolean isOnlineMode() {
		return DedicatedServer.properties.getBooleanProperty("online-mode", true);
	}
	
	public static void setOnlineMode(boolean onlineMode) {
		DedicatedServer.properties.setProperty("online-mode", onlineMode);
	}
	
	public static String getMotD() {
		return DedicatedServer.properties.getStringProperty("motd", "A Minecraft Server");
	}
	
	public static void setMotD(String motd) {
		DedicatedServer.properties.setProperty("motd", motd);
	}
	
	public static String getSeed() {
		return DedicatedServer.properties.getStringProperty("level-seed", "");
	}
	
	public static void setSeed(String seed) {
		DedicatedServer.properties.setProperty("level-seed", seed);
	}
	
	public static String getLevelType() {
		return DedicatedServer.properties.getStringProperty("level-type", "DEFAULT");
	}
	
	public static void setLevelType(String levelType) {
		DedicatedServer.properties.setProperty("level-type", levelType);
	}
	
	public static String getGeneratorSettings() {
		return DedicatedServer.properties.getStringProperty("generator-settings", "");
	}
	
	public static void setGeneratorSettings(String generatorSettings) {
		DedicatedServer.properties.setProperty("generator-settings", generatorSettings);
	}
	
	public static int getMaxBuildHeight() {
		return DedicatedServer.properties.getIntProperty("max-build-height", 256);
	}
	
	public static void setMaxBuildHeight(int maxBuildHeight) {
		DedicatedServer.properties.setProperty("max-build-height", maxBuildHeight);
	}
	
	public static boolean getEnableQuery() {
		return DedicatedServer.properties.getBooleanProperty("enable-query", false);
	}
	
	public static void setEnableQuery(boolean enableQuery) {
		DedicatedServer.properties.setProperty("enable-query", enableQuery);
	}
	
	public static boolean getEnableRCON() {
		return DedicatedServer.properties.getBooleanProperty("enable-rcon", false);
	}
	
	public static void setEnableRCON(boolean enableRCON) {
		DedicatedServer.properties.setProperty("enable-rcon", enableRCON);
	}
	
	public static int getDifficulty() {
		return DedicatedServer.properties.getIntProperty("difficulty", 1);
	}
	
	public static void setDifficulty(int difficulty) {
		DedicatedServer.properties.setProperty("difficulty", difficulty);
	}
	
	public static boolean getHardcore() {
		return DedicatedServer.properties.getBooleanProperty("hardcore", false);
	}
	
	public static void setHardcore(boolean hardcore) {
		DedicatedServer.properties.setProperty("hardcore", hardcore);
	}
	
	public static boolean getAllowNether() {
		return DedicatedServer.properties.getBooleanProperty("allow-nether", true);
	}
	
	public static void setAllowNether(boolean allowNether) {
		DedicatedServer.properties.setProperty("allow-nether", allowNether);
	}
	
	public static boolean getSnooperEnabled() {
		return DedicatedServer.properties.getBooleanProperty("snooper-enabled", true);
	}
	
	public static void setSnooperEnabled(boolean snooperEnabled) {
		DedicatedServer.properties.setProperty("snooper-enabled", snooperEnabled);
	}
}
