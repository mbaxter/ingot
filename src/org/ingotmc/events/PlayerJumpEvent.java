package org.ingotmc.events;

import java.util.ArrayList;

import org.ingotmc.api.Player;

public class PlayerJumpEvent extends Event {
	public static ArrayList<EventListener> listeners = new ArrayList<EventListener>();
	private Player p;
	
	public PlayerJumpEvent() {}
	
	public PlayerJumpEvent(Player p) {
		this.p = p;
	}
	
	public Player getPlayer() {
		return p;
	}
	
	public void addListener(EventListener l) {
		listeners.add(l);
	}
	
	public ArrayList<EventListener> getListeners() {
		return listeners;
	}
}
