package org.ingotmc.events;

import java.util.ArrayList;

import org.ingotmc.api.Player;

public class PlayerLoginEvent extends Event {
	public static ArrayList<EventListener> listeners = new ArrayList<EventListener>();
	private Player p;
	private String r;
	
	public PlayerLoginEvent() {}
	
	public PlayerLoginEvent(Player p) {
		this.p = p;
	}
	
	public Player getPlayer() {
		return p;
	}
	
	public void setCancelReason(String r) {
		this.r = r;
	}
	
	public String getCancelReason() {
		return r;
	}
	
	public void addListener(EventListener l) {
		listeners.add(l);
	}
	
	public ArrayList<EventListener> getListeners() {
		return listeners;
	}
}
