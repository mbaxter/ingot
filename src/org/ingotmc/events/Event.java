package org.ingotmc.events;

import java.util.ArrayList;

public abstract class Event {
	public abstract void addListener(EventListener l);
	public abstract ArrayList<EventListener> getListeners();
}
