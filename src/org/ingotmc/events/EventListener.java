package org.ingotmc.events;

public interface EventListener {
	public Event handleEvent(Event e);
}