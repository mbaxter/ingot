package org.ingotmc.events;

import java.util.ArrayList;

import org.ingotmc.api.Player;

public class PlayerChatEvent extends CancellableEvent {
	public static ArrayList<EventListener> listeners = new ArrayList<EventListener>();
	private Player p;
	private String m;
	
	public PlayerChatEvent() {}
	
	public PlayerChatEvent(Player p, String m) {
		this.p = p;
		this.m = m;
	}
	
	public void setMessage(String m) {
		this.m = m;
	}
	
	public Player getPlayer() {
		return p;
	}
	
	public String getMessage() {
		return m;
	}
	
	public void addListener(EventListener l) {
		listeners.add(l);
	}
	
	public ArrayList<EventListener> getListeners() {
		return listeners;
	}
}
