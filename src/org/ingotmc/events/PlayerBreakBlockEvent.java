package org.ingotmc.events;

import java.util.ArrayList;

import org.ingotmc.api.Block;
import org.ingotmc.api.Player;

public class PlayerBreakBlockEvent extends CancellableEvent {
	public static ArrayList<EventListener> listeners = new ArrayList<EventListener>();
	private Player p;
	private Block b;
	
	public PlayerBreakBlockEvent() {}
	
	public PlayerBreakBlockEvent(Player p, Block b) {
		this.p = p;
		this.b = b;
	}
	
	public Player getPlayer() {
		return p;
	}
	
	public Block getBlock() {
		return b;
	}
	
	public void addListener(EventListener l) {
		listeners.add(l);
	}
	
	public ArrayList<EventListener> getListeners() {
		return listeners;
	}
}
