package org.ingotmc.events;

import java.util.ArrayList;

public abstract class CancellableEvent extends Event {
	boolean cancelled = false;
	
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
	
	public boolean isCancelled() {
		return cancelled;
	}
	
	public abstract void addListener(EventListener l);
	public abstract ArrayList<EventListener> getListeners();
}
