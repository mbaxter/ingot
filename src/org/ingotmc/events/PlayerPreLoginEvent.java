package org.ingotmc.events;

import java.util.ArrayList;


public class PlayerPreLoginEvent extends CancellableEvent {
	public static ArrayList<EventListener> listeners = new ArrayList<EventListener>();
	private String r, p;
	
	public PlayerPreLoginEvent() {}
	
	public PlayerPreLoginEvent(String p) {
		this.p = p;
	}
	
	public String getPlayerName() {
		return p;
	}
	
	public void setCancelReason(String r) {
		this.r = r;
	}
	
	public String getCancelReason() {
		return r;
	}
	
	public void addListener(EventListener l) {
		listeners.add(l);
	}
	
	public ArrayList<EventListener> getListeners() {
		return listeners;
	}
}
