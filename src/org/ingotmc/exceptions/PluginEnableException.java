package org.ingotmc.exceptions;

import java.util.logging.Level;

import org.ingotmc.Ingot;

public class PluginEnableException extends PluginException {
	private static final long serialVersionUID = 4658245573829880699L;

	public PluginEnableException(String name) {
		super(name, null);
		Ingot.log("Exception while enabling plugin " + getPluginName(), Level.WARNING);
	}
}
