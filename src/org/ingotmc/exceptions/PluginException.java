package org.ingotmc.exceptions;

import java.io.File;

public class PluginException extends Exception {
	private static final long serialVersionUID = 1L;
	protected String name;
	protected File file;
	
	public PluginException(String name, File file) {
		this.name = name;
		this.file = file;
	}
	
	public String getPluginName() {
		return name;
	}
	
	public File getPluginFile() {
		return file;
	}
}
