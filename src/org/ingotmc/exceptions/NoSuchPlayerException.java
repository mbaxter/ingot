package org.ingotmc.exceptions;

public class NoSuchPlayerException extends PlayerException {
	private static final long serialVersionUID = 6579611838072790254L;

	public NoSuchPlayerException(String name) {
		super(name);
	}
}
