package org.ingotmc.exceptions;

public class PluginNotFoundException extends PluginException {
	private static final long serialVersionUID = -5333385278284923767L;
	
	public PluginNotFoundException(String name) {
		super(name, null);
	}
}
