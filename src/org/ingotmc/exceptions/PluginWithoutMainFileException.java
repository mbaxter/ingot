package org.ingotmc.exceptions;

import java.io.File;

public class PluginWithoutMainFileException extends PluginException {
	private static final long serialVersionUID = -588638542737282256L;
	
	public PluginWithoutMainFileException(String name, File file) {
		super(name, file);
	}
}
