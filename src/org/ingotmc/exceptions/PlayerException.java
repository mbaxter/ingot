package org.ingotmc.exceptions;

public class PlayerException extends Exception {
	private static final long serialVersionUID = 1L;
	protected String name;
	
	public PlayerException(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
