package org.ingotmc;

public enum DeathCause {
	HIT_BY_PLAYER, HIT_BY_MOB, FALLEN, BLOWN_UP, UNKNOWN;
}
