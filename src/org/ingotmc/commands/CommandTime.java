package org.ingotmc.commands;

import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.ingotmc.ChatColor;
import org.ingotmc.Ingot;
import org.ingotmc.api.Command;
import org.ingotmc.api.CommandSender;
import org.ingotmc.api.Player;
import org.ingotmc.exceptions.NoSuchPlayerException;

public class CommandTime extends Command {

	@Override
	public String getCommandName() {
		return "time";
	}

	@Override
	public int getRequiredArgumentsLength() {
		return 0;
	}

	@Override
	public String getCommandUsage() {
		return "/time <set/add/remove> <ticks>";
	}

	@Override
	public void processCommand(CommandSender s, String[] a) {
		if(!s.isPlayer()) {
			s.sendMessage(ChatColor.RED + "Command must be run as player!");
		}
		Player p = null;
		try {
			p = Ingot.getPlayer(s.getName());
		} catch (NoSuchPlayerException e) {}
		
		if(a.length < 1) {
			s.sendMessage(ChatColor.GREEN + "Time: " + ChatColor.RESET + p.getLocation().getWorld().getTime() + " ticks.");
			s.sendMessage(getCommandUsage());
			return;
		}
		else if(a.length < 2) {
			s.sendMessage(ChatColor.RED + "Usage: " + getCommandUsage());
			return;
		}
		else {
			if(!p.isOp()) {
				s.sendMessage(ChatColor.RED + "You don't have permission to do this!");
				return;
			}
		}
		if(a[0].equalsIgnoreCase("set")) {
			if(NumberUtils.isNumber(a[1])) {
				p.getLocation().getWorld().setTime(Long.parseLong(a[1]));
				Ingot.notifyAdmins(s.getName(), "set time of \"" + p.getLocation().getWorld().getName() + "\" to " + a[1] + " ticks");
			}
			else {
				s.sendMessage(ChatColor.RED + a[1] + " must be number!");
			}
		}
		else if(a[0].equalsIgnoreCase("add")) {
			if(NumberUtils.isNumber(a[1])) {
				long ticks = p.getLocation().getWorld().getTime() + Long.parseLong(a[1]);
				p.getLocation().getWorld().setTime(ticks);
				Ingot.notifyAdmins(s.getName(), "set time of \"" + p.getLocation().getWorld().getName() + "\" to " + ticks + " ticks");
			}
			else {
				s.sendMessage(ChatColor.RED + a[1] + " must be number!");
			}
		}
		else if(a[0].equalsIgnoreCase("remove")) {
			if(NumberUtils.isNumber(a[1])) {
				long ticks = p.getLocation().getWorld().getTime() - Long.parseLong(a[1]);
				p.getLocation().getWorld().setTime(ticks);
				Ingot.notifyAdmins(s.getName(), "set time of \"" + p.getLocation().getWorld().getName() + "\" to " + ticks + " ticks");
			}
			else {
				s.sendMessage(ChatColor.RED + a[1] + " must be number!");
			}
		}
		else {
			s.sendMessage(ChatColor.RED + "Usage: " + getCommandUsage());
		}
	}

	@Override
	public List<String> getCommandAliases() {
		return null;
	}
	
	@Override
	public String getCommandDescription() {
		return "Get and set the world time";
	}
}
