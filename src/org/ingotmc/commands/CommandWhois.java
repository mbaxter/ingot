package org.ingotmc.commands;

import java.util.ArrayList;
import java.util.List;

import org.ingotmc.ChatColor;
import org.ingotmc.api.Command;
import org.ingotmc.api.CommandSender;
import org.ingotmc.api.GameMode;
import org.ingotmc.Ingot;
import org.ingotmc.api.Player;
import org.ingotmc.exceptions.NoSuchPlayerException;

public class CommandWhois extends Command {
	@Override
	public String getCommandName() {
		return "whois";
	}

	@Override
	public String getCommandUsage() {
		return "/whois <player>";
	}

	@Override
	public void processCommand(CommandSender s, String[] a) {
		Player p;
		try {
			p = Ingot.getPlayer(a[0]);
		} catch (NoSuchPlayerException e) {
			s.sendMessage(ChatColor.RED + "No such player " + e.getName());
			return;
		}
		s.sendMessage(ChatColor.GOLD + "Information about " + a[0] + ":");
		s.sendMessage(ChatColor.GRAY + "- Arrows In Player: " + ChatColor.RESET + p.getArrowCount());
		s.sendMessage(ChatColor.GRAY + "- Chat Visibility: " + ChatColor.RESET + p.getChatVisibility());
		s.sendMessage(ChatColor.GRAY + "- IP Address: " + ChatColor.RESET + p.getIP());
		s.sendMessage(ChatColor.GRAY + "- Held Item: " + ChatColor.RESET + getHand(p));
		s.sendMessage(ChatColor.GRAY + "- Game Mode: " + ChatColor.RESET + getMode(p));
		s.sendMessage(ChatColor.GRAY + "- Can Fly: " + ChatColor.RESET + getFlightAllowed(p));
		s.sendMessage(ChatColor.GRAY + "- Is Flying: " + ChatColor.RESET + getFlying(p));
		s.sendMessage(ChatColor.GRAY + "- Is Op: " + ChatColor.RESET + getOp(p));
		s.sendMessage(ChatColor.GRAY + "- Location: " + ChatColor.RESET + p.getLocation().getWorld().getName() + ", " + p.getLocation().getX() + ", " + p.getLocation().getY() + ", " + p.getLocation().getZ());
	}

	@Override
	public List<String> getCommandAliases() {
		List<String> aliases = new ArrayList<String>();
		aliases.add("playerinfo");
		return aliases;
	}

	@Override
	public int getRequiredArgumentsLength() {
		return 1;
	}
	
	@Override
	public String getCommandDescription() {
		return "Gets info of the player";
	}
	
	private String getHand(Player p) {
		if(p.getHeldItem() == null) return "none";
		return p.getHeldItem().getDisplayName();
	}
	
	private String getMode(Player p) {
		if(p.getGameMode().equals(GameMode.CREATIVE)) return "Creative";
		return "Survival/Adventure";
	}
	
	private String getFlightAllowed(Player p) {
		if(p.getFlyingAllowed()) return "Yes";
		return "No";
	}
	
	private String getFlying(Player p) {
		if(p.getFlying()) return "Yes";
		return "No";
	}
	
	private String getOp(Player p) {
		if(p.isOp()) return "Yes";
		return "No";
	}
}
