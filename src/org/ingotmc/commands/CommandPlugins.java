package org.ingotmc.commands;

import java.util.ArrayList;
import java.util.List;

import org.ingotmc.ChatColor;
import org.ingotmc.api.Command;
import org.ingotmc.api.CommandSender;
import org.ingotmc.Ingot;
import org.ingotmc.Plugin;

public class CommandPlugins extends Command {
	@Override
	public String getCommandName() {
		return "plugins";
	}

	@Override
	public int getRequiredArgumentsLength() {
		return 0;
	}

	@Override
	public String getCommandUsage() {
		return "/plugins";
	}

	@Override
	public void processCommand(CommandSender s, String[] a) {
		int l = Ingot.getPlugins().length;
		String plural = l > 1 ? "s" : "";
		
		StringBuilder sb = new StringBuilder(ChatColor.GREEN + l + " Plugin" + plural + ": " + ChatColor.RESET);
		for(Plugin p : Ingot.getPlugins()) {
			sb.append(p.getName() + ", ");
		}
		sb.setLength(sb.length() - 2);
		sb.append(".");
		
		s.sendMessage(sb.toString());
	}

	@Override
	public List<String> getCommandAliases() {
		List<String> list = new ArrayList<String>(); list.add("pl"); list.add("pluginlist");
		return list;
	}
	
	@Override
	public String getCommandDescription() {
		return "Lists all installed plugins";
	}
}
