package org.ingotmc.commands;

import java.util.List;

import org.ingotmc.ChatColor;
import org.ingotmc.Ingot;
import org.ingotmc.api.Command;
import org.ingotmc.api.CommandSender;

public class CommandHelp extends Command {
	@Override
	public String getCommandName() {
		return "help";
	}
	
	@Override
	public int getRequiredArgumentsLength() {
		return 0;
	}
	
	@Override
	public String getCommandUsage() {
		return "/help";
	}
	
	@Override
	public void processCommand(CommandSender s, String[] a) {
		s.sendMessage(ChatColor.GREEN + "All commands:");
		for(Command c : Ingot.getCommands())
			s.sendMessage(ChatColor.GRAY + "/" + c.getCommandName() + ChatColor.RESET + " " + c.getCommandDescription());
	}
	
	@Override
	public List<String> getCommandAliases() {
		return null;
	}
	
	@Override
	public String getCommandDescription() {
		return "Lists all commands";
	}
}
