package org.ingotmc;

import java.io.File;

public class PluginManager {
	public static void init() {
		for(File f : new File("plugins").listFiles()) {
			if(f.getName().endsWith(".jar")) {
				try {
					new Plugin(f).enable();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
