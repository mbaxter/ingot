 package org.ingotmc;

import java.util.ArrayList;
import java.util.logging.Level;

import org.ingotmc.api.Command;
import org.ingotmc.api.Player;
import org.ingotmc.api.PropertiesManager;
import org.ingotmc.events.Event;
import org.ingotmc.events.EventListener;
import org.ingotmc.exceptions.NoSuchPlayerException;
import org.ingotmc.exceptions.PluginNotFoundException;

import net.minecraft.server.*;
import net.minecraft.src.*;

public class Ingot extends PropertiesManager {
	public static ILogAgent logger;
	
	private static String version = "19", mcVersion = "1.6.4";
	private static int protocolVersion = 78;
	private static ArrayList<Plugin> plugins = new ArrayList<Plugin>();
	private static ArrayList<Command> commands = new ArrayList<Command>();
	
	public static void addPlugin(Plugin p) {
		for(Plugin pl : plugins)
			if(pl.getName().equalsIgnoreCase(p.getName()))
				return;
		plugins.add(p);
	}
	
	public static String getVersion() {
		return version;
	}
	
	public static void registerListener(EventListener l, Event e) {
		e.addListener(l);
	}
	
	public static Event callEvent(Event e) {
		Event u = null;
		for(EventListener l : e.getListeners()) {
			u = l.handleEvent(e);
		}
		if(u == null) return e;
		return u;
	}
	
	public static int getProtocolVersion() {
		return protocolVersion;
	}
	
	public static void notifyAdmins(String pl, String didWhat) {
		for(Player p : getOnlinePlayers()) {
			if(p.isOp()) {
				p.sendMessage(ChatColor.GRAY + ChatColor.ITALIC + "[" + pl + ": " + didWhat + "]");
			}
		}
	}
	
	public static String getMinecraftVersion() {
		return mcVersion;
	}
	
	public static void log(String message, Level level) {
		if(level.equals(Level.INFO))
			logger.func_98233_a(message);
		else if(level.equals(Level.WARNING))
			logger.func_98236_b(message);
	}
	
	public static void registerCommand(Command cmd) {
		for(Command co : commands)
			if(co.getCommandName().equalsIgnoreCase(cmd.getCommandName()))
				return;
		commands.add(cmd);
		ServerCommandManager.instance.registerCommand(cmd);
	}
	
	public static Player getPlayer(String name) throws NoSuchPlayerException {
		Player p = new Player(MinecraftServer.getServer().getConfigurationManager().getPlayerEntity(name));
		if(p.getNMS() == null) throw new NoSuchPlayerException(name);
		return p;
	}
	
	public static Player[] getOnlinePlayers() {
		ArrayList<Player> players = new ArrayList<Player>();
		for(String player : MinecraftServer.getServer().getAllUsernames()) {
			try {
				players.add(getPlayer(player));
			} catch (NoSuchPlayerException e) {}
		}
		return players.toArray(new Player[players.size()]);
	}
	
	public static void broadcastMessage(String message) {
		for(Player p : getOnlinePlayers())
			p.sendMessage(message);
	}
	
	public static Plugin[] getPlugins() {
		return plugins.toArray(new Plugin[plugins.size()]);
	}
	
	public static Plugin getPlugin(String name) throws PluginNotFoundException {
		for(Plugin p : plugins)
			if(p.getName().equalsIgnoreCase(name))
				return p;
		throw new PluginNotFoundException(name);
	}
	
	public static Command[] getCommands() {
		return commands.toArray(new Command[commands.size()]);
	}
	
	public static Command getCommand(String name) {
		for(Command c : commands)
			if(c.getCommandName().equalsIgnoreCase(name))
				return c;
		return null;
	}
}
