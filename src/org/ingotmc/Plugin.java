package org.ingotmc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.ingotmc.exceptions.PluginEnableException;
import org.ingotmc.exceptions.PluginNotFoundException;
import org.ingotmc.exceptions.PluginWithoutMainFileException;

public class Plugin {
	private File f;
	private Object instance;
	private static final Class<?>[] parameters = new Class[]{URL.class};
	
	public Plugin(File f) {
		this.f = f;
		Ingot.addPlugin(this);
	}
	
	public String getName() {
		return f.getName().replace(".jar", "");
	}
	
	public Object getInstance() {
		return instance;
	}
	
	public File getFile() {
		return f;
	}
	
	@SuppressWarnings("deprecation")
	public void enable() throws Exception {
		boolean hasMain = false;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ZipInputStream in = null;
		
		try {
			try {
				in = new ZipInputStream(new FileInputStream(f));
			} catch (FileNotFoundException e) {
				throw new PluginNotFoundException(getName());
			}
			ZipEntry entry;
			while((entry = in.getNextEntry()) != null) {
				if(entry.getName().equals("main.txt")) {
					hasMain = true;
					byte[] buf = new byte[1024];
					int len;
					while((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
				}
			}
		} finally {
			if(in != null) {
				in.close();
			}
			out.close();
			
			if(!hasMain)
				throw new PluginWithoutMainFileException(getName(), f);
		}
		String mainClass = new String(out.toByteArray());
		addUrl(f.toURL());
		Class<?> clazz = Class.forName(mainClass);
		Constructor<?> ctor = clazz.getConstructor();
		Object object = ctor.newInstance();
		instance = object;
	}
	
	private void addUrl(URL u) throws IOException, PluginEnableException {
		URLClassLoader loader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		Class<URLClassLoader> sysClass = URLClassLoader.class;
		
		try {
			Method method = sysClass.getDeclaredMethod("addURL", parameters);
			method.setAccessible(true);
			method.invoke(loader, new Object[]{u});
		} catch (Throwable t) {
			t.printStackTrace();
			throw new PluginEnableException(getName());
		}
	}
}
